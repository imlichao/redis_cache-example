package pub.imlichao;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;

@Controller
public class CacheTest {
    /**
     * 缓存测试方法延时两秒
     * @param i
     * @return
     */
    @Cacheable(value = "pub.imlichao.CacheTest.cacheFunction",key="#i")
    public String cacheFunction(int i){
        try {
            long time = 2000L;
            Thread.sleep(time);
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }

        return "success"+ i;
    }
}
