package pub.imlichao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Date;

@Controller
public class HelloController {
    @Autowired
    CacheTest cacheTest;

    @GetMapping(value = "/")
    public String hello(){
        return "/hello";
    }

    @GetMapping(value = "/cache_test")
    public String cache_test(){
        for(int i=0;i<5;i++){
            System.out.println(new Date() + " " + cacheTest.cacheFunction(i));
        }
        return "/hello";
    }
}
