package pub.imlichao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication // 包含了 @Configuration @EnableAutoConfiguration @ComponentScan 三个注解
@EnableCaching //启用Spring cache声明式缓存
public class Application {
    public static void main(String[] args) {
        //运行spring boot web项目
        SpringApplication.run(Application.class, args);
    }
}